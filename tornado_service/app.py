#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tornado.ioloop
import tornado.web

import json
from tornado.web import RedirectHandler

##############################################
# global declaration

global_dummy_counter = 0
global_html_template = "template not loaded" #default error msg

with open("./app_template.html") as f:
    global_html_template = f.read() # fill variable with file content, use like template

##############################################
# tornado handler and route

class AppIndex(tornado.web.RequestHandler):
    def get(self):
        """Default page for show: content-type = text/html"""
        global global_dummy_counter #use global var like example data storage

        self.set_header("Content-Type", "text/html")
        self.set_header("Access-Control-Allow-Origin","*") # for file:// debug

        cb = "AppCounter: "
        cb += str(global_dummy_counter)

        page_buffer = global_html_template.format(title="Counter page", content=cb)
        # make template 

        self.write(page_buffer)

class AppCounterJson(tornado.web.RequestHandler):
    def get(self):
        """GET request for data show, return json"""

        global global_dummy_counter

        self.set_header("Content-Type", "application/json")
        self.set_header("Access-Control-Allow-Origin","*") # for file:// debug

        cb_data = {"counter": global_dummy_counter, "status": "ready"}
        cb = json.dumps(cb_data, indent=4) # nice json output

        self.write(cb)

class AppCounterIncrJson(tornado.web.RequestHandler):
    def get(self):
        """GET request for data increase, return json"""
        global global_dummy_counter
        global_dummy_counter += 1

        self.set_header("Content-Type", "application/json")
        self.set_header("Access-Control-Allow-Origin","*")

        cb_data = {"counter": global_dummy_counter, "status": "incr"} #example data output
        cb = json.dumps(cb_data, indent=4) # nice json output

        self.write(cb)

def make_app():
    """Route map"""
    return tornado.web.Application([
        (r"/", AppIndex),
        (r"/counter", AppCounterJson),
        (r"/counter/incr", AppCounterIncrJson),
        (r"/static/(.*)", tornado.web.StaticFileHandler, {"path": "./static/"}),
        (r"/favicon.ico", RedirectHandler, {"url":"static/favicon.ico"}),
    ])

##############################################
# startup loop

if __name__ == "__main__":
    app = make_app()
    app.listen(9000)
    tornado.ioloop.IOLoop.current().start()
