# Требования

Технические задачи:

* Tornado Web Server
* * Handler:
* * * / - for index
* * * /tap - cor increase counter
* Electron desktop app for tornado service
* * show static html
* * show rest request via JS
* Script for:
* * install
* * deploy

Platform:

* *Ubuntu 18.04 x86_64 GNU/Linux*
* *Python 3.6*
* *node v8.10.0*
* *Сервис работает на той же машине, что и приложение electron (localhost)*

tornado_service
~~~~
pip3 install tornado
cd tornado_service
./tornado_service.sh
~~~~

electron_app
~~~~
npm install
npm start
~~~~

## Time reporting:

* **init** Четверг, 05. декабря 2019 18:17 
* **pause** Четверг, 05. декабря 2019 19:18 ~ 1 час

* **continue** Четверг, 05. декабря 2019 22:28 
* **ending** Четверг, 05. декабря 2019 23:36 ~ 1 час

**Итого: 2 часа + 20 мин документация и выгрузка на git**

