var func_incr = function(){
    console.log("func_incr()");

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
            document.getElementById("xhr_container").innerHTML = xmlHttp.responseText;
            console.log(xmlHttp.responseText);
        }            
    }
    xmlHttp.open("GET", "http://localhost:9000/counter/incr", true);
    xmlHttp.send(null);

    func_common();
};

var func_common = function(){
    var iframe = document.getElementById("iframe_container");
    iframe.src = iframe.src;
};

document.addEventListener('DOMContentLoaded', function(){
    console.log("DOMContentLoaded");

    document.getElementById("btn_incr").addEventListener("click", function(evt){ func_incr(); });

    func_common();
});